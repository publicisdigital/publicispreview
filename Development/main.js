function init(){

	console.log("INIT")

  // adjustPanels()
  // window.addEventListener("resize", adjustPanels)

	initJSON();

}

function eventListeners(){

	console.log("Setting Buttons");

}

//NOT CURRENTLY BEING USED PLUS PLEASE LEAVE IN INCASE NEEDED///////////////////////////////////////////////////////////////////////////////

function adjustPanels(){
  var leftPanelWidth = document.querySelector("#leftPanel").offsetWidth;
  console.log("LEFT PANEL WIDTH:", leftPanelWidth);
  // document.querySelector("#mainContent").style.width = "calc(100% - " + leftPanelWidth + "px";
  // document.querySelector("#downloadBtn").style.width = "calc(100% - " + leftPanelWidth + "px";
  // document.querySelector("#iFrameHolder").style.width = "calc(100% - " + leftPanelWidth + "px";
  // document.querySelector("#iFrameHolder").style.width = "100%";
  // document.querySelector("#downloadBtn").style.width = "78%";
  
  var mainPanelWidth = document.querySelector("#mainContent").offsetWidth;
  // var downloadBtnWidth = document.querySelector("#downloadBtn").offsetWidth;

  // console.log("LEFT PANEL WIDTH:", leftPanelWidth, "MAIN PANEL WIDTH:", mainPanelWidth, "DOWNLOAD BTN WIDTH:", downloadBtnWidth);

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function loadJSON(callback) {   

    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'pageContent.json', true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
            parseJSON();
          }
    };
    xobj.send(null);  
 }

var myJSON;
 function initJSON() {
   loadJSON(function(response) {
    // Parse JSON string into object
      myJSON = JSON.parse(response);
      console.log(myJSON)
   });
}

var numVersions;
var version;
var currentID = 0;
function parseJSON(){

  numVersions = myJSON.content.length
  console.log("NUMBER OF VERSIONS:", numVersions)//, "VERSION:", version[0])
  // console.log("MY JSON:", myJSON.content[1][0])

  currentVersion = myJSON.content[versionNumber]

  createVersions();
  loadInitialBanner(currentVersion,currentID);

}

var firstRun = true;
var currentVersion;
var versionNumber = 0;
function createVersions(){

  if(numVersions <= 1){
    document.querySelector("#versionHolder").style.display = "none"
  }else{
    document.querySelector("#versionHolder").style.display = "block"
    for(var i=0; i<numVersions; i++){

      var div = document.createElement("div");
      document.querySelector(".vBlockHolder").appendChild(div);
      div.classList.add('vBlock');

      var txtDiv = document.createElement("div");
      div.appendChild(txtDiv);
      txtDiv.classList.add('vBlockTxt');
      txtDiv.innerHTML = "V"+(i+1)+"";
      // txtDiv.style.textDecoration = "underline";

      TweenMax.from(div, 0.5, {delay:(i/20), alpha:0, x:"-=10"})

      div.style.cursor = "pointer";

      try{throw i}
      catch(ii) {
          div.addEventListener("click", function(){
            versionNumber = ii;
            currentID = ii;
            removeSizes();
            currentVersion = myJSON.content[ii][0];
            createSizes(myJSON.content[ii])
            animateOutFrame();
            removeSelectedVersions();
            selectedVersion(versionNumber);
            removeSelectedSizes();
            selectedSize(0);
          })
      }

    }    
  }

  if(firstRun){
    createSizes(currentVersion);
    if(numVersions>1){
      selectedVersion(versionNumber);
    }
    selectedSize(currentID);
    firstRun = false;
  }

}

function removeSelectedVersions(){
  for(var i=0; i<numVersions; i++){
    childVersionElements[i].children[0].style.textDecoration = "none";
  }
}

function removeSelectedSizes(){
  for(var i=0; i<currentVersionNumSizes; i++){
    childSizeElements[i].children[0].style.textDecoration = "none";
  }
}

var childVersionElements;
function selectedVersion(currentVersionNumber){

  childVersionElements = document.querySelector(".vBlockHolder").children;
  var childVersionTxt = childVersionElements[currentVersionNumber].children[0];
  childVersionTxt.style.textDecoration = "underline";

}

var childSizeElements;
function selectedSize(currentVersion){

  childSizeElements = document.querySelector(".sBlockHolder").children;
  var childSizeTxt = childSizeElements[currentVersion].children[0];
  console.log("CURRENT SIZE TEXT:", childSizeTxt)
  childSizeTxt.style.textDecoration = "underline";

}

function removeSizes(){

  var childElements = document.getElementById("sHolder").children;
  var parentElement = document.getElementById("sHolder");

  while(childElements.length){
    childElements[0].parentElement.removeChild(childElements[0]);
  }

}

var sizeDiv;
var txtDiv;
var currentVersionNumSizes;
function createSizes(thisVersion){

    console.log("CURRENT VERSION:", thisVersion)

    currentVersionNumSizes = thisVersion.length;

    for(var i=0; i<currentVersionNumSizes; i++){
      // console.log(i)
      sizeDiv = document.createElement("div");
      document.querySelector(".sBlockHolder").appendChild(sizeDiv);
      sizeDiv.classList.add('sBlock');

      txtDiv = document.createElement("div");
      sizeDiv.appendChild(txtDiv);
      txtDiv.classList.add('sBlockTxt');
      txtDiv.innerHTML = "" + thisVersion[i].width + "x" + thisVersion[i].height;

      TweenMax.from(sizeDiv, 0.5, {delay:(i/15), alpha:0, y:"-=12"})

      sizeDiv.style.cursor = "pointer";

      try{throw i}
      catch(ii) {
        sizeDiv.addEventListener("click", function(){
          removeSelectedSizes();
          selectedSize(ii);
          animateOutFrame();
          currentVersion = thisVersion[ii];
          currentID = ii;
        })
      }

    }    
}

function removeButtonEvents(){

  //REMOVING VERSION BUTTON EVENTS//////////////////

  var childVersionElements = document.querySelector(".vBlockHolder").children;

  for(var i=0; i<childVersionElements.length; i++){
    childVersionElements[i].style.pointerEvents = "none"
  }

  /////////////////////////////////////////////////

  //REMOVING SIZE BUTTON EVENTS//////////////////

  var childSizeElements = document.querySelector(".sBlockHolder").children;

  for(var i=0; i<childSizeElements.length; i++){
    childSizeElements[i].style.pointerEvents = "none"
  }

  /////////////////////////////////////////////////

}

function addingButtonEvents(){
  
  //ADDING VERSION BUTTON EVENTS//////////////////

  var childVersionElements = document.querySelector(".vBlockHolder").children;

  for(var i=0; i<childVersionElements.length; i++){
    childVersionElements[i].style.pointerEvents = "auto"
  }

  /////////////////////////////////////////////////

  //ADDING SIZE BUTTON EVENTS//////////////////

  var childSizeElements = document.querySelector(".sBlockHolder").children;

  for(var i=0; i<childSizeElements.length; i++){
    childSizeElements[i].style.pointerEvents = "auto"
  }

  /////////////////////////////////////////////////

}

function animateOutFrame(){
  removeButtonEvents();
  TweenMax.to(iframe, 0.3, {alpha:0, y:"+=15", overwrite:0, onComplete:animateInFrame})
}

function animateInFrame(){
  loadNewBanner(currentVersion,currentID)
  // setDownload(currentVersion)
  TweenMax.set(iframe, {y:"-=30", overwrite:0})
  TweenMax.to(iframe, 0.3, {delay:0.1, alpha:1, y:"+=15", overwrite:0, onComplete:addingButtonEvents})
}

var myTitle;
var iframe;
var frameHolder;
function loadInitialBanner(thisVersion,thisID){

  console.log("THIS VERSION:", thisVersion)

  iframe = document.createElement('iframe');
  frameHolder = document.querySelector("#iFrameHolder");
 
  iframe.src = thisVersion[thisID].loc;
  
  frameHolder.appendChild(iframe);
  frameHolder.style.width = ""+thisVersion[thisID].width+"px";
  frameHolder.style.height = ""+thisVersion[thisID].height+"px";
  iframe.style.width = ""+thisVersion[thisID].width+"px";
  iframe.style.height = ""+thisVersion[thisID].height+"px";

  TweenMax.from(iframe, 0.4, {delay:0.1, alpha:0, y:"-=15"})

  myTitle = document.querySelector("#contentTitle")
  // myTitle.innerHTML = thisVersion[thisID].title;

  myTitle.innerHTML = thisVersion[thisID].title + " - " + thisVersion[thisID].width + "x" + thisVersion[thisID].height;

  // setDownload(thisVersion[thisID])

}

function loadNewBanner(thisVersion,thisID){

  console.log("thisVersion:", thisVersion, "thisID:", thisID)

  myTitle.innerHTML = thisVersion.title + " - " + thisVersion.width + "x" + thisVersion.height;

  iframe.src = "";
  iframe.style.width = ""+thisVersion.width+"px";
  iframe.style.height = ""+thisVersion.height+"px";
  frameHolder.style.width = ""+thisVersion.width+"px";
  frameHolder.style.height = ""+thisVersion.height+"px";
  iframe.src = thisVersion.loc;

}

function setDownload(thisVersion){
  // console.log("MY ZIP FILE", thisVersion.loc)
  newDownloadString = thisVersion.loc.replace('index.html','');
  console.log("MY ZIP FILE", ""+newDownloadString+"")

  document.querySelector("#downloadItem").getAttribute("href");
  var myHref = document.querySelector("#downloadItem")
  myHref.href = ""+newDownloadString+"";
  myHref.download = ""+ thisVersion.title +""+thisVersion.width+"x"+thisVersion.height;
  console.log("MY NEW HREF:", myHref)

  /*var zip = new JSZip();
  var a = document.querySelector("a");
  var urls = [newDownloadString];

  function request(url) {
    return new Promise(function(resolve) {
      var httpRequest = new XMLHttpRequest();
      httpRequest.open("GET", url);
      httpRequest.onload = function() {
        zip.file(""+this.responseText+"");
        resolve()
      }
      httpRequest.send()
      })
    }

    Promise.all(urls.map(function(url) {
      return request(url)
    }))
    .then(function() {
      console.log("MY ZIP:", zip);
      zip.generateAsync({
        type: "blob"
    })
    .then(function(content) {
      a.download = ""+ thisVersion.title +""+thisVersion.width+"x"+thisVersion.height;
      a.href = URL.createObjectURL(content);
      a.innerHTML = "Download Source";
    });
  })*/
}


